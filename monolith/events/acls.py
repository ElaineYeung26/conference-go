from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    query = {"query": f"{city}, {state}"}
    r = requests.get(url, headers=headers, params = query)
    location_pic = json.loads(r.text)
    return location_pic["photos"][0]["src"]["original"]

def convert_to_lat_long(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    query = {
        "q": f"{city}, {state}, US",
        "appid": OPEN_WEATHER_API_KEY,
        }
    r = requests.get(url, params = query)
    location_lat_lon = json.loads(r.text)

    if len(location_lat_lon) == 0:
        return None, None
    else:
        return location_lat_lon[0]["lat"], location_lat_lon[0]["lon"]

def get_weather(lat, lon):
    url = "https://api.openweathermap.org/data/2.5/weather"
    query = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    r = requests.get(url, params = query)

    if r.status_code == 200:
        location_weather = json.loads(r.text)
        return location_weather["main"]["temp"], location_weather["weather"][0]["description"]
    else:
        return None, None
